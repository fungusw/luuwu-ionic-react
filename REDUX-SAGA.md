```
npm install react-redux
npm install redux-saga
npm install axios
npm install reduxsauce
npm install redux-logger
```

- In the /src folder, Rename index.tsx to index.js
- In the /src folder, Rename App.tsx to App.jsx , then replace the following code to Provider and PersistGate

```
---
const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route path="/home" component={Home} exact={true} />
        <Route exact path="/" render={() => <Redirect to="/home" />} />
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);
---

+++
import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import createStore from './Stores';
import RootScreen from './Containers/Root/RootScreen';

const { store, persistor } = createStore();

const App = () => (
  <Provider store={store}>
      {/**
       * PersistGate delays the rendering of the app's UI until the persisted state has been retrieved
       * and saved to redux.
       * The `loading` prop can be `null` or any react instance to show during loading (e.g. a splash screen),
       * for example `loading={<SplashScreen />}`.
       * @see https://github.com/rt2zz/redux-persist/blob/master/docs/PersistGate.md
       */}
      <PersistGate loading={null} persistor={persistor}>
        <RootScreen />
      </PersistGate>
    </Provider>
);
+++
```

- In the /src folder, create or copy /Stores, /Sagas and /Services folder