# Luuwu Ionic React Testing #
[Installation Get Started](https://ionicframework.com/docs/react/quickstart) 


1) install ionic cli 
```
npm install -g @ionic/cli
```
2) create project
```
ionic start ProjectName blank --type=react
```
## PAGES
[PAGES.md](./PAGES.md)

## react-redux, redux-saga and axios
[REDUX-SAGA.md](./REDUX-SAGA.md)

## redux-persist
[REDUX-PERSIST.md](./REDUX-PERSIST.md)

## connected-react-router
[CONNECTED-REACT-ROUTER.md](./CONNECTED-REACT-ROUTER2.md)

## Formik, Yup, PropTypes, and Tree-Changes

```
npm install formik
npm install yup
npm install prop-types
npm install tree-changes
```
