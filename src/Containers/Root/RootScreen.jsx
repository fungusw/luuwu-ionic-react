import React, {PureComponent} from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { history } from '../../Stores/CreateStore';
import { ConnectedRouter } from 'connected-react-router';
import Home from '../../pages/Home/Home';
import Item from '../../pages/Item/Item';
import Customer from '../../pages/Customer/Customer';
import CustomerDetail from '../../pages/CustomerDetail/CustomerDetail';
import ItemDetail from '../../pages/ItemDetail/ItemDetail';
import Profile from '../../pages/Profile/Profile';
import AppActions from '../../Stores/App/Actions';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import '../../theme/variables.css';

class RootScreen extends PureComponent {
    componentDidMount() {
      const {startup, pathname} = this.props;
      // Run the startup saga when the application is starting
      startup(pathname);
    }
  
    render() {
      const { appPath } = this.props;
      return (
        <IonApp>
          <ConnectedRouter history={history}>
            <IonRouterOutlet>
              <Route path={`${appPath}/profile`} component={Profile} exact={true} />
              <Route path={`${appPath}/itemDetail`} component={ItemDetail} exact={true} />
              <Route path={`${appPath}/customerDetail`} component={CustomerDetail} exact={true} />
              <Route path={`${appPath}/customer`} component={Customer} exact={true} />
              <Route path={`${appPath}/item`} component={Item} exact={true} />
              <Route path={`${appPath}/home`} component={Home} exact={true} />
              <Route exact={true} path={`${appPath}/`} render={() => <Redirect to={`${appPath}/home`} />} />
            </IonRouterOutlet>
          </ConnectedRouter>
        </IonApp>
      );
    }
  }
  
  RootScreen.propTypes = {
    // startup: PropTypes.func,
  };
  
  RootScreen.defaultProps = {
    // startup: () => {},
  };
  
  const mapStateToProps = state => ({
    pathname: state.router.location.pathname,
    appPath: state.app.appPath,
  });
  
  const mapDispatchToProps = dispatch => ({
    startup: () => dispatch(AppActions.startUp()),
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(RootScreen);