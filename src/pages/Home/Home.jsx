import { 
  IonContent,
   IonHeader,
  IonPage, 
  IonTitle, 
  IonToolbar, 
  isPlatform, 
  IonItem, 
  IonInput, 
  IonList, 
  IonButton, 
  IonToast, 
  IonGrid, 
  IonCol, 
  IonRow, 
  IonAlert, 
  IonLabel,
} from '@ionic/react';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';
// import ExploreContainer from '../components/ExploreContainer';
import './Home.css';
import AppActions from '../../Stores/App/Actions';
import { logo } from '../../theme/image';

class Home extends PureComponent {
  constructor() {
    super();

    this.state = {
      username: '',
      password: '',
      event: false,
      install: false,
      installed: false,
      alert: false,
      displayMode: '',
    }
    this.button = this.button.bind(this);
    this.setShowAlert1 = this.setShowAlert1.bind(this);
    this.setShowInstallToast = this.setShowInstallToast.bind(this);
  }

  componentDidMount() {
    // const { resetToInitialState } = this.props;
    // resetToInitialState();
    window.addEventListener('beforeinstallprompt', (e) => {
      this.setState({event: true});
      e.preventDefault();
      this.deferreedPrompt=e;
    })
    window.addEventListener('appinstalled', (evt) => {
      // Log install to analytics
      this.setState({installed: true});
    });
    window.addEventListener('DOMContentLoaded', () => {
      if (navigator.standalone) {
        this.setState({displayMode: 'standalone-ios'});
      }else if (window.matchMedia('(display-mode: standalone)').matches) {
        this.setState({displayMode: 'standalone'});
      }else if(isPlatform('ios')) {
        this.setState({alert: true});
      }else {
        this.setState({displayMode: 'browser'});
      }
    });
  }

  setShowInstallToast(ios) {
    this.setState({ios});
  }

  setShowAlert1(alert) {
    this.setState({alert});
  }

  button() {
    if (this.deferreedPrompt !== undefined && this.deferreedPrompt !== null) {
      this.deferreedPrompt.prompt();
      this.deferreedPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted'){
          this.setState({installed: true});
          console.log('User accepted the A2HS prompt');
        }
        this.deferreedPrompt = null;
      })
    }
  }

  render () {
    const {install, alert, displayMode, installed, event}= this.state;
    const {authenticate, successMessage, setShowSuccessToast, showSuccessToast, setShowErrorToast, showErrorToast, errorMessage, apiUrl} = this.props;
    const initialValues = {
      username: '',
      password: '',
      apiUrl
    };
    console.log('install', install);
    console.log('alert', alert);
    console.log('displayMode', displayMode);
    console.log('installed', installed);
    console.log('event', event);
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonGrid>
              <IonRow>
                <IonCol class="align-center">
                  <IonTitle>Login</IonTitle>
                </IonCol>
                {displayMode === 'browser' && !installed && event &&
                  <IonCol class="ion-text-end">
                    <IonButton onClick={() => this.button()}>
                      INSTALL
                    </IonButton>
                  </IonCol>
                }
              </IonRow>
            </IonGrid>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Login</IonTitle>
            </IonToolbar>
          </IonHeader>
          <div className="ion-text-center">
            <img alt="img" src={logo} className="logo" />
          </div>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            onSubmit={(values, formikBag) => {
              authenticate(formikBag, values.username, values.password, values.apiUrl);
            }}
            validationSchema={Yup.object().shape({
              username: Yup.string().required('username_is_required'),
              password: Yup.string()
                .required('password_is_required')
                .nullable(),
            })}
          >
            {({values, handleChange, errors, isSubmitting, isValid, handleSubmit, setFieldValue}) => (
              <>
                <IonList>
                  <IonItem>
                    <IonLabel position="floating">Username</IonLabel>
                    <IonInput value={values.username} onIonChange={e => setFieldValue('username',e.detail.value)}></IonInput>
                  </IonItem>
                  <IonItem>
                    <IonLabel position="floating">Password</IonLabel>
                    <IonInput type="password" value={values.password} onIonChange={e =>setFieldValue('password', e.detail.value)}></IonInput>
                  </IonItem>
                  <IonItem>
                    <IonLabel position="floating">Url</IonLabel>
                    <IonInput value={values.apiUrl} onIonChange={e =>setFieldValue('apiUrl', e.detail.value)}></IonInput>
                  </IonItem>
                  <IonButton onClick={handleSubmit} expand="block" class="ion-margin">Login</IonButton>
                </IonList>
              </>
            )}
          </Formik>
          {/* <ExploreContainer /> */}
          <IonToast
            isOpen={showSuccessToast}
            onDidDismiss={() => setShowSuccessToast(false)}
            message={successMessage}
            duration={2000}
          />
          <IonToast
            isOpen={showErrorToast}
            onDidDismiss={() => setShowErrorToast(false)}
            message={errorMessage}
            duration={2000}
          />
          <IonToast
            isOpen={install}
            onDidDismiss={() => this.setShowInstallToast(false)}
            message={'Added To Home Screen'}
            duration={2000}
          />
          <IonAlert
            isOpen={alert}
            onDidDismiss={() => this.setShowAlert1(false)}
            header={'Luuwu Ionic'}
            subHeader={'Add this app to your home screen for easy access and a better experience.'}
            message={'Tap the share ccon then "Add to Home Screen"'}
            buttons={['OK']}
          />
        </IonContent>
      </IonPage>
    );
  }
};

const mapStateToProps = state => ({
  successMessage: state.app.successMessage,
  showSuccessToast: state.app.showSuccessToast,
  showErrorToast: state.app.showErrorToast,
  errorMessage: state.app.errorMessage,
  apiUrl: state.app.apiUrl,
  // username: state.app.username,
  // password: state.app.password,
});

const mapDispatchToProps = dispatch => ({
  authenticate: (formikBag, username, password, apiUrl) =>
    dispatch(AppActions.appAuthenticate(formikBag, username, password, apiUrl)),
  setShowSuccessToast: showSuccessToast => dispatch(AppActions.setShowSuccessToast(showSuccessToast)),
  setShowErrorToast: showErrorToast => dispatch(AppActions.setShowErrorToast(showErrorToast)),
  resetToInitialState: () => dispatch(AppActions.resetToInitialState()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
