import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  IonPage,
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent, 
  IonCard, 
  IonCardHeader, 
  IonCardTitle, 
  IonCardContent,
  IonText,
  IonButton,
  IonIcon,
  IonLabel,
  IonButtons,
  IonItem,
  IonAvatar,
} from '@ionic/react';
import { logOutOutline, arrowBack } from 'ionicons/icons';
import AppActions from '../../Stores/App/Actions';
import { smallLogo } from '../../theme/image';

class Profile extends PureComponent {
  constructor() {
    super();

    this.state = {}
  }

  componentDidMount() {}

  render () {
    const { user, logout, goToBack } = this.props;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonButton shape="round" onClick={() => goToBack('customer')}><IonIcon icon={arrowBack} />BACK</IonButton>
            </IonButtons>
            <IonItem>
              <IonAvatar style={{"--border-radius":10}}>
                <img src={smallLogo} alt="small-logo"/>
              </IonAvatar>
              <IonTitle>
                Profile
              </IonTitle>
            </IonItem>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle>
                Profile
              </IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonCard>
            <IonCardHeader>
              <IonCardTitle>
                {user.username}
              </IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
              <IonText>
                <p>
                  {user.email}
                </p>
              </IonText>
              <IonText>
                <p>
                  {user.firstname}
                </p>
              </IonText>
              <IonText>
                <p>
                  {user.lastname}
                </p>
              </IonText>
              <IonButton onClick={() => logout()}>
                <IonLabel>Logout</IonLabel>
                <IonIcon icon={logOutOutline}/>
              </IonButton>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonPage>
    );
  }
}

const mapStateToProps = state => ({
  user: state.app.user,
  appPath: state.app.appPath,
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(AppActions.logout()),
  goToBack: path => dispatch(AppActions.goToBack(path)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);