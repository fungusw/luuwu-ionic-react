import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent, 
  IonSlides, 
  IonSlide, 
  IonText, 
  IonButton, 
  IonIcon, 
  IonCard, 
  IonCardContent, 
  IonCardHeader, 
  IonCardTitle, 
  IonCardSubtitle, 
  IonButtons,
  IonItem,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonModal,
  IonThumbnail,
  IonInput,
  IonGrid,
  IonRow,
  IonCol,
  IonBadge,
  IonToast,
  IonList,
  IonItemDivider,
  IonFooter,
  IonAvatar,
} from '@ionic/react';
import * as Yup from 'yup';
import { Formik, Field } from 'formik';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import SlsOrdDetailActions from '../../Stores/SlsOrdDetail/Actions';
import AppActions from '../../Stores/App/Actions';
import { noPhotoMedium, smallLogo } from '../../theme/image';
import { addCircleOutline, removeCircleOutline, close, cartOutline, informationCircleOutline, cartSharp, arrowBack } from 'ionicons/icons';
import { siteFlowId } from '../../Services/Config';
import './ItemDetail.css';
import OrderListItem from '../../components/ListItem/OrderListItem';


class ItemDetail extends PureComponent {
  constructor() {
    super();

    this.state = {
      index: 0,
      showModal: false,
      quantity: 0,
      showCart: false,
    }

    this.setShowModal = this.setShowModal.bind(this);
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
    this.useCreateDetail = this.useCreateDetail.bind(this);
    this.extractValue = this.extractValue.bind(this);
    this.handleChangeUom = this.handleChangeUom.bind(this);
    this.checkValue = this.checkValue.bind(this);
    this.setShowCart = this.setShowCart.bind(this);
  }

  componentDidMount() {
    const {
      resetItemPhotos,
      fetchItemPhotos,
      itemId,
      itemPhotosSorts,
      itemPhotosFilters,
      itemPhotosPageSize,
      fetchUomOptions,
      fetchLocationOptions
    } = this.props;
    // fetch item first page
    resetItemPhotos();
    fetchItemPhotos(itemId, 1, itemPhotosSorts, itemPhotosFilters, itemPhotosPageSize);
    fetchUomOptions(itemId, '');
    fetchLocationOptions(siteFlowId, '');
    this.useCreateDetail();
  }

  setShowModal(showModal) {
    this.setState({showModal});
  }

  setShowCart(showCart) {
    this.setState({showCart});
  }

  increment() {
    const {quantity} = this.state;
    const quantityInt = parseInt(quantity, 10);
    let incQ = quantityInt + 1;
    this.setState({quantity: incQ});
  }

  decrement() {
    const {quantity} = this.state;
    const quantityInt = parseInt(quantity, 10);
    let decQ = quantityInt - 1;
    this.setState({quantity: decQ});
  }

  useCreateDetail() {
    const {initDocumentDetail, setInitDocumentDetail, itemId, itemDesc01, itemDesc02} = this.props;
    setInitDocumentDetail(initDocumentDetail, itemId, itemDesc01, itemDesc02);
  }

  extractValue(options, value) {
    if(options) {
      const index = options.map(option => {return option.value}).indexOf(value);
      return {label:options[index].label, value};
    }
    return null;
  }

  handleChangeUom(form, value) {
    const {hdrId, changeUom, documentDetail} = this.props;
    changeUom(form, hdrId, documentDetail.item_id, value);
  }

  checkValue(value) {
    if(isNaN(value)){
      return 1;
    }
    let numberValue = parseInt(value, 10);
    console.log('numberValue', numberValue);
    // check max value
    if (numberValue >= 999) {
      numberValue = 999;
    }

    // check min value
    if (numberValue <= 1) {
      numberValue = 1;
    }
    return numberValue;
  }

  render () {
    const { index, showModal, quantity, showCart } = this.state;
    const {
      itemPhotos,
      itemPhotosLength, 
      itemPhotosIsLoading, 
      uomOptions, 
      locationOptions, 
      itemDesc01, 
      documentDetailsLength, 
      createDetail, 
      hdrId, 
      documentDetail,
      showSuccessToast, 
      setShowSuccessToast, 
      successMessage, 
      showErrorToast, 
      setShowErrorToast, 
      errorMessage,
      documentDetails,
      documentHeader,
      goToBack,
    } = this.props
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonButton shape="round" onClick={() => goToBack('item')}><IonIcon icon={arrowBack} />BACK</IonButton>
            </IonButtons>
              <IonItem>
                <IonAvatar style={{"--border-radius":10}}>
                  <img src={smallLogo} alt="small-logo"/>
                </IonAvatar>
                <IonTitle>
                  Item Details
                </IonTitle>
              </IonItem>
            <IonButtons slot="end">
              <IonButton onClick={() => this.setShowCart(true)} shape="round" fill="clear" color="primary">
                <IonIcon size="large" icon={cartOutline} />
              </IonButton>
              <IonBadge id="notifications-badge">{documentDetailsLength}</IonBadge>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                {itemPhotosLength > 0 && !itemPhotosIsLoading && (
                  <IonSlides
                    pager={true}
                    onIonSlidePrevEnd={() => {this.setState({index: index-1})}} 
                    onIonSlideNextEnd={() => {this.setState({index: index+1})}}
                  >
                    {itemPhotos.map(itemPhoto => {
                      return (
                        <IonSlide>
                          <img src={noPhotoMedium} alt='img'/>
                        </IonSlide>
                      );
                    })}
                  </IonSlides>
                )}
                {itemPhotosLength <= 0 && !itemPhotosIsLoading && (
                  <IonSlides pager={true}>
                    <IonSlide>
                      <img src={noPhotoMedium} alt='img'/>
                    </IonSlide>
                  </IonSlides>
                )}
              </IonCol>
              <IonCol size="12">
                {itemPhotosLength <= 0 && !itemPhotosIsLoading && (
                  <IonCard>
                    <IonCardHeader>
                      <IonCardTitle><h2>{itemDesc01}</h2></IonCardTitle>
                      <IonCardSubtitle><h2>Price</h2></IonCardSubtitle>
                      <IonCardSubtitle><h2>Desc 01</h2></IonCardSubtitle>
                    </IonCardHeader>
                    <IonCardContent>
                      <IonText>
                        <p>Description - Desc 02</p>
                      </IonText>
                    </IonCardContent>
                  </IonCard>
                )}
                {itemPhotosLength > 0 && !itemPhotosIsLoading && (
                  <IonCard>
                    <IonCardHeader>
                      <IonCardTitle><h2>{itemDesc01}</h2></IonCardTitle>
                      <IonCardSubtitle><h2>Price</h2></IonCardSubtitle>
                      <IonCardSubtitle><h2>{itemPhotos[index].desc_01}</h2></IonCardSubtitle>
                    </IonCardHeader>
                    <IonCardContent>
                      <IonText>
                        <p>{itemPhotos[index].desc_02}</p>
                      </IonText>
                    </IonCardContent>
                  </IonCard>
                )}
              </IonCol>
            </IonRow>
          </IonGrid>
          <Formik
            initialValues={documentDetail}
            onSubmit={(values, formikBag) => {
              console.log('values', values);
              // remove the submit_action field
              const processedValues = {...values};
              delete processedValues.submit_action;
              if (values.id > 0) {
                // updateDetails(formikBag, hdrId, [processedValues]);
              } else if (!values.id) {
                createDetail(formikBag, hdrId, processedValues);
              }
              this.setShowModal(false);
            }}
            validationSchema={
              Yup.object().shape({
              location_select2: Yup.object().shape({
                value: Yup.number().min(1, ('location_is_required')),
              }),
              qty: Yup.number().moreThan(0, ('qty_must_be_greater_than_0')),
              uom_select2: Yup.object().shape({
                value: Yup.number().min(1, ('uom_is_required')),
              }),
            })}
          >
            {({values, errors, isSubmitting, handleSubmit, setFieldValue}) => {
              console.log(values.uom_select2);
            return (
              <IonModal onDidDismiss={() => this.setShowModal(false)} isOpen={showModal}>
                <div>
                  <IonItem>
                  <IonGrid>
                    <IonRow>
                      <IonCol class="ion-align-self-center" size="auto">
                        <IonThumbnail><img src={noPhotoMedium} alt="img"/></IonThumbnail>
                      </IonCol>
                      <IonCol>
                        <p>{itemDesc01}</p>
                      </IonCol>
                      <IonCol size="auto">
                        <IonButton fill="clear" onClick={() => this.setShowModal(false)}><IonIcon icon={close}/></IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                  </IonItem>
                  <IonGrid>
                    <IonRow>
                      <IonCol>
                        <IonItem>
                          <IonLabel>Quantity :</IonLabel>
                          <IonItem slot="end">
                            <IonButton fill="clear" onClick={() => this.decrement()}>
                              <IonIcon icon={removeCircleOutline} />
                            </IonButton>
                            <IonItem>
                              <IonInput
                                onIonChange={e => {
                                  const value = this.checkValue(e.detail.value);
                                  this.setState({quantity: value});
                                  setFieldValue('qty', value);
                                }}
                                min={1}
                                max={999}
                                size="3"
                                value={quantity}
                              />
                            </IonItem>
                            <IonButton fill="clear" onClick={() => this.increment()}>
                              <IonIcon icon={addCircleOutline} />
                            </IonButton>
                          </IonItem>
                        </IonItem>
                        {typeof errors === 'object' && errors.qty &&  
                          <IonText color="danger">
                            <IonIcon icon={informationCircleOutline} />
                            {errors.qty}
                          </IonText>
                        }
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol size="12" sizeMd="6">
                        <IonItem>
                          <IonLabel>UOM :</IonLabel>
                          <Field>
                            {({form}) => (
                              <IonSelect
                                  interface="alert"
                                  onIonChange={e => {
                                    this.handleChangeUom(form, e.detail.value);
                                    const value = this.extractValue(uomOptions, e.detail.value);
                                    if(value) {
                                      setFieldValue('uom_select2', value);
                                    }
                                  }}
                              >
                                {uomOptions.map(uomOption => {
                                  return (
                                    <IonSelectOption key={uomOption.value} value={uomOption.value}>{uomOption.label}</IonSelectOption>
                                  )
                                })}
                              </IonSelect>
                            )}
                          </Field>
                        </IonItem>
                        {typeof errors === 'object' && errors.uom_select2 &&  
                          <IonText color="danger">
                            <IonIcon icon={informationCircleOutline} />
                            {errors.uom_select2.value}
                          </IonText>
                        }
                      </IonCol>
                      {values.uom_select2.value !== 0 && (
                        <IonCol size="12" sizeMd="6">
                          <IonItem>
                            <IonLabel>UOM Rate :</IonLabel>
                            <IonLabel class="ion-text-right" slot="end">{values.uom_rate}</IonLabel>
                          </IonItem>
                        </IonCol>
                      )}
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <IonItem>
                          <IonLabel>Location :</IonLabel>
                          <IonSelect
                            interface="alert"
                            onIonChange={e => {
                              const value = this.extractValue(locationOptions, e.detail.value);
                              if(value) {
                                setFieldValue('location_select2', value);
                              }
                            }}
                          >
                            {locationOptions.map(locationOptions => {
                              return (
                                <IonSelectOption key={locationOptions.value} value={locationOptions.value}>{locationOptions.label}</IonSelectOption>
                              )
                            }
                            )}
                          </IonSelect>
                        </IonItem>
                        {typeof errors === 'object' && errors.location_select2 &&  
                          <IonText color="danger">
                            <IonIcon icon={informationCircleOutline} />
                            {errors.location_select2.value}
                          </IonText>
                        }
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </div>
                <div>
                <IonGrid>
                  <IonRow>
                    <IonCol>
                      <IonItem>
                        <IonLabel>Price :</IonLabel>
                        <IonInput></IonInput>
                      </IonItem>
                    </IonCol>
                    <IonCol class="ion-text-right" onClick={handleSubmit}>
                      <IonButton>Add Order </IonButton>
                    </IonCol>
                  </IonRow>
                </IonGrid>
                </div>
              </IonModal>
            )}}
          </Formik>
          <IonToast
            isOpen={showSuccessToast}
            onDidDismiss={() => setShowSuccessToast(false)}
            message={successMessage}
            duration={2000}
          />
          <IonToast
            isOpen={showErrorToast}
            onDidDismiss={() => setShowErrorToast(false)}
            message={errorMessage}
            duration={2000}
          />
          <IonModal onDidDismiss={() => this.setShowCart(false)} isOpen={showCart}>
            <IonContent>
              <IonList>
                <IonItemDivider sticky>
                  <IonGrid>
                    <IonRow>
                      <IonCol>
                        <h1>
                          {`${documentHeader.doc_code}'s Order`}
                        </h1>
                      </IonCol>
                      <IonCol class="ion-align-self-center" size="auto">
                        <IonButton fill="clear" onClick={() => this.setShowCart(false)}><IonIcon icon={close}/></IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonItemDivider>
                {documentDetails.map(documentDetail => {
                  return (
                    <OrderListItem key={documentDetail.id} item={documentDetail} />
                  )
                })}
              </IonList>
            </IonContent>
          </IonModal>
        </IonContent>
        <IonFooter>
          <IonToolbar>
            <IonGrid class="ion-text-right">
              <IonButton onClick={() => this.setShowModal(true)}>
                <IonIcon icon={cartSharp} />
                <IonLabel>
                  Add
                </IonLabel>
              </IonButton>
            </IonGrid>
          </IonToolbar>
        </IonFooter>
      </IonPage>
    );
  }
}

const mapStateToProps = state => ({
  itemId: state.itemDetails.itemId,
  itemPhotos: state.itemDetails.itemPhotos,
  itemPhotosLength: state.itemDetails.itemPhotos.length,
  itemPhotosIsLoading: state.itemDetails.itemPhotosIsLoading,
  itemPhotosSorts: state.itemDetails.itemPhotosSorts,
  itemPhotosFilters: state.itemDetails.itemPhotosFilters,
  itemPhotosCurrentPage: state.itemDetails.itemPhotosCurrentPage,
  itemPhotosPageSize: state.itemDetails.itemPhotosPageSize,
  itemDesc01: state.itemDetails.itemDesc01,
  itemDesc02: state.itemDetails.itemDesc02,
  uomOptions: state.slsOrdDetail.uomOptions,
  locationOptions: state.slsOrdDetail.locationOptions,
  documentDetailsLength: state.slsOrdDetail.documentDetails.length,
  hdrId: state.outbOrd01.hdrId,
  documentDetail: state.slsOrdDetail.documentDetail,
  initDocumentDetail: state.slsOrdDetail.initDocumentDetail,
  successMessage: state.itemDetails.successMessage,
  showSuccessToast: state.itemDetails.showSuccessToast,
  showErrorToast: state.itemDetails.showErrorToast,
  errorMessage: state.itemDetails.errorMessage,
  documentHeader: state.slsOrdDetail.documentHeader,
  documentDetails: state.slsOrdDetail.documentDetails,
  appPath: state.app.appPath,
});

const mapDispatchToProps = dispatch => ({
  setShowSuccessToast: showSuccessToast => dispatch(ItemDetailsActions.setShowSuccessToast(showSuccessToast)),
  setShowErrorToast: showErrorToast => dispatch(ItemDetailsActions.setShowErrorToast(showErrorToast)),
  resetItemPhotos: () => dispatch(ItemDetailsActions.resetItemPhotos()),
  fetchItemPhotos: (itemId, page, sorts, filters, pageSize) =>
    dispatch(ItemDetailsActions.fetchItemPhotos(itemId, page, sorts, filters, pageSize)),
  fetchUomOptions: (itemId, search) =>
    dispatch(SlsOrdDetailActions.slsOrdDetailFetchUomOptions(itemId, search)),
  fetchLocationOptions: (siteFlowId, search) =>
    dispatch(SlsOrdDetailActions.slsOrdDetailFetchLocationOptions(siteFlowId, search)),
  createDetail: (formikBag, hdrId, documentDetail, pendingIndex) =>
    dispatch(
      SlsOrdDetailActions.slsOrdDetailCreateDetail(formikBag, hdrId, documentDetail, pendingIndex),
    ),
  setInitDocumentDetail: (documentDetail, item_id, desc_01, desc_02) =>
    dispatch(
      SlsOrdDetailActions.slsOrdDetailSetInitDocumentDetail(
        documentDetail,
        item_id,
        desc_01,
        desc_02,
      ),
    ),
  changeUom: (formikBag, hdrId, itemId, uomId) =>
    dispatch(SlsOrdDetailActions.slsOrdDetailChangeUom(formikBag, hdrId, itemId, uomId)),
  goToBack: path => dispatch(AppActions.goToBack(path)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);