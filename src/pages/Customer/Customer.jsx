import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent, 
  IonList, 
  IonInfiniteScroll, 
  IonInfiniteScrollContent, 
  IonSearchbar,
  IonButton,
  IonLabel,
  IonAvatar,
  IonItem,
  IonIcon,
  // IonFooter,
  // IonGrid,
  // IonRow,
  // IonCol,
} from '@ionic/react';
import OutbOrd01Actions from '../../Stores/OutbOrd01/Actions';
import AppActions from '../../Stores/App/Actions';
import CustomerListItem from '../../components/ListItem/CustomerListItem';
import { smallLogo } from '../../theme/image';
import { personOutline } from 'ionicons/icons';
import './Customer.css';

class Customer extends PureComponent {
  constructor(props) {
    super(props);

    const { filterDeliveryPoint } = this.props;
    this.state = {
      searchText: filterDeliveryPoint,
    }
    this.useOnEndReached = this.useOnEndReached.bind(this);
    this.setSearchText = this.setSearchText.bind(this);
    this.processFeature = this.processFeature.bind(this);
  }

  componentDidMount() {
    const {resetDocument, fetchDocument, currentPage, sorts, filters, pageSize} = this.props;
    resetDocument(currentPage, sorts, filters, pageSize);
    fetchDocument(1, 1, sorts, filters, pageSize);
  }

  componentDidUpdate(prevProps) {
    const {resetDocument, fetchDocument, currentPage, sorts, filters, pageSize} = this.props;
    if (prevProps.filters !== filters) {
      resetDocument();
      fetchDocument(1, currentPage, sorts, filters, pageSize);
    }
  }

  useOnEndReached(e) {
    const {
      fetchIsLoading,
      currentPage,
      fetchDocument,
      sorts,
      filters,
      pageSize,
      documentsLength,
    } = this.props;
    // increase the page
    // data less then 5 no need to fetch.
    if (fetchIsLoading === false && documentsLength > 5) {
      // problem with fetching with only one or two item.
      fetchDocument(1, currentPage + 1, sorts, filters, pageSize, e);
    }
  }

  setSearchText(searchText) {
    this.setState({searchText});
  }

  processFeature(value) {
    const {setFilters} = this.props;
    const delivery_point = value;
    setFilters({delivery_point});
  }

  render () {
    const { documents, username, goToProfile } = this.props;
    const { searchText } = this.state;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonItem>
              <IonAvatar style={{"--border-radius":10}}>
                <img src={smallLogo} alt="small-logo"/>
              </IonAvatar>
              <IonTitle>
                Customer
              </IonTitle>
            </IonItem>
            <IonButton fill="clear" slot="end" onClick={() => {goToProfile()}}>
              <IonLabel class="label">{username}</IonLabel>
              <IonIcon icon={personOutline}/>
            </IonButton>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonSearchbar
            placeholder="Search by Location"
            value={searchText}
            type="search"
            onKeyUp={e => {
              if(e.key === 'Enter'){
                this.processFeature(searchText);
              }
            }}
            onIonClear={() => {
              this.setSearchText('');
              this.processFeature('');
            }}
            onIonChange={e => this.setSearchText(e.detail.value)}
          />
          <IonList>
            {documents.map(document => {
              return (
                <CustomerListItem key={document.id} item={document} />
              )
            })}
            <IonInfiniteScroll threshold="100px" onIonInfinite={(e) => this.useOnEndReached(e)}>
              <IonInfiniteScrollContent
                  loadingText="Loading more good products...">
              </IonInfiniteScrollContent>
            </IonInfiniteScroll>
          </IonList>
        </IonContent>
        {/* <IonFooter>
          <IonToolbar>
            <IonGrid>
              <IonRow>
                <IonCol class="ion-text-right">
                  <IonButton>
                    ADD CUSTOMER
                  </IonButton>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonToolbar>
        </IonFooter> */}
      </IonPage>
    );
  }
}

const mapStateToProps = state => ({
  fetchIsLoading: state.outbOrd01.fetchIsLoading,
  documentsLength: state.outbOrd01.documents.length,
  documents : state.outbOrd01.documents,
  sorts: state.outbOrd01.sorts,
  filters: state.outbOrd01.filters,
  currentPage: state.outbOrd01.currentPage,
  pageSize: state.outbOrd01.pageSize,
  username: state.app.user.username,
  filterDeliveryPoint: state.outbOrd01.filters.delivery_point,
});

const mapDispatchToProps = dispatch => ({
  resetDocument: (currentPage, sorts, filters, pageSize) =>
    dispatch(OutbOrd01Actions.outbOrd01ResetTimestamp(currentPage, sorts, filters, pageSize)),
  fetchDocument: (divisionId, currentPage, sorts, filters, pageSize, e) =>
    dispatch(
      OutbOrd01Actions.outbOrd01FetchOutbOrd01(divisionId, currentPage, sorts, filters, pageSize, e),
    ),
  setFilters: filters => dispatch(OutbOrd01Actions.outbOrd01SetFilters(filters)),
  goToProfile: () => dispatch(AppActions.goToProfile()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Customer);