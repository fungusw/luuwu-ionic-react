import React from 'react';
import { 
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonList, 
  IonContent, 
  IonInfiniteScroll, 
  IonInfiniteScrollContent,
  IonSearchbar,
  IonButtons,
  IonButton,
  IonBadge,
  IonIcon,
  IonText,
  IonModal,
  IonGrid,
  IonRow,
  IonCol,
  IonItemDivider,
  IonItem,
  IonAvatar,
} from '@ionic/react';
import { connect } from 'react-redux';
import { cartOutline, close, arrowBack } from 'ionicons/icons';
import ItemActions from '../../Stores/Item/Actions';
import AppActions from '../../Stores/App/Actions';
import ItemListItem from '../../components/ListItem/ItemListItem';
import OrderListItem from '../../components/ListItem/OrderListItem';
import { smallLogo } from '../../theme/image';


class Item extends React.PureComponent {
  constructor(props) {
    super(props);

    const { filterDesc01 } = this.props;

    this.state = {
      searchText: filterDesc01,
      showCart: false,
    }
    this.useOnEndReached = this.useOnEndReached.bind(this);
    this.setSearchText = this.setSearchText.bind(this);
    this.processFeature = this.processFeature.bind(this);
    this.setShowCart = this.setShowCart.bind(this);
  }

  componentDidMount() {
    const { resetItems, fetchItems, sorts, filters, pageSize } = this.props;
    resetItems();
    fetchItems(1, sorts, filters, pageSize);
  }

  componentDidUpdate(prevProps) {
    const {
      resetItems,
      fetchItems,
      sorts,
      filters,
      pageSize,
    } = this.props;

    if (prevProps.filters !== filters) {
      resetItems();
      fetchItems(1, sorts, filters, pageSize);
    }
  }

  useOnEndReached(e) {
    const {
      itemsIsLoading,
      currentPage,
      fetchItems,
      sorts,
      filters,
      pageSize,
      itemsLength,
    } = this.props;
    // increase the page
    // data less then 5 no need to fetch.
    if (itemsIsLoading === false && itemsLength > 5) {
      fetchItems(currentPage + 1, sorts, filters, pageSize, e);
    }
  }

  setSearchText(searchText) {
    this.setState({searchText});
  }

  processFeature(desc_01) {
    if (desc_01 || desc_01 === '') {
      const {setFilters} = this.props;
      // setFeatures(features);
      setFilters({desc_01});
    }
  }

  setShowCart(showCart) {
    this.setState({showCart});
  }

  render() {
    const { items, documentDetailsLength, documentHeader, documentDetails, goToBack } = this.props;
    const { searchText, showCart } = this.state;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonButton shape="round" onClick={() => goToBack('customerDetail')}><IonIcon icon={arrowBack} />BACK</IonButton>
            </IonButtons>
            <IonItem>
              <IonAvatar style={{"--border-radius":10}}>
                <img src={smallLogo} alt="small-logo"/>
              </IonAvatar>
              <IonTitle>
                Item
              </IonTitle>
            </IonItem>
            <IonButtons slot="end">
              <IonText>{documentHeader.doc_code}</IonText>
              <IonButton onClick={() => this.setShowCart(true)} shape="round" fill="clear" color="primary"><IonIcon size="large" icon={cartOutline} /></IonButton>
              <IonBadge id="notifications-badge">{documentDetailsLength}</IonBadge>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonSearchbar
            placeholder="Search by Name"
            value={searchText}
            type="search"
            onKeyUp={e => {
              if(e.key === 'Enter'){
                this.processFeature(searchText);
              }
            }}
            onIonClear={() => {
              this.setSearchText('');
              this.processFeature('');
            }}
            onIonChange={e => this.setSearchText(e.detail.value)}
          />
          {/*-- List of Text Items --*/}
          <IonList>
            {items.map((item, index) => {
              return (
                <ItemListItem key={index} item={item} />
              );
            })}
            <IonInfiniteScroll threshold="100px" onIonInfinite={(e) => this.useOnEndReached(e)}>
              <IonInfiniteScrollContent
                  loadingText="Loading more good products...">
              </IonInfiniteScrollContent>
            </IonInfiniteScroll>
          </IonList>
          <IonModal onDidDismiss={() => this.setShowCart(false)} isOpen={showCart}>
            <IonContent>
              <IonList>
                <IonItemDivider sticky>
                  <IonGrid>
                    <IonRow>
                      <IonCol>
                        <h1>
                          {`${documentHeader.doc_code}'s Order`}
                        </h1>
                      </IonCol>
                      <IonCol class="ion-align-self-center" size="auto">
                        <IonButton fill="clear" onClick={() => this.setShowCart(false)}><IonIcon icon={close}/></IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonItemDivider>
                {documentDetails.map(documentDetail => {
                  return (
                    <OrderListItem key={documentDetail.id} item={documentDetail} />
                  )
                })}
              </IonList>
            </IonContent>
          </IonModal>
        </IonContent>
      </IonPage>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.app.token,
    items: state.item.items,
    itemsIsLoading: state.item.itemsIsLoading,
    sorts: state.item.sorts,
    filters: state.item.filters,
    currentPage: state.item.currentPage,
    pageSize: state.item.pageSize,
    itemsLength: state.item.items.length,
    successMessage: state.item.successMessage,
    errorMessage: state.item.errorMessage,
    documentDetailsLength: state.slsOrdDetail.documentDetails.length,
    documentHeader: state.slsOrdDetail.documentHeader,
    documentDetails: state.slsOrdDetail.documentDetails,
    filterDesc01: state.item.filters.desc_01,
    appPath: state.app.appPath,
  }
};

const mapDispatchToProps = dispatch => ({
  resetItems: () => dispatch(ItemActions.resetItems()),
  fetchItems: (page, sorts, filters, pageSize, e) =>
    dispatch(ItemActions.fetchItems(page, sorts, filters, pageSize, e)),
  setFilters: filters => dispatch(ItemActions.setFilters(filters)),
  // resetFilters: () => dispatch(ItemActions.resetFilters()),
  goToBack: path => dispatch(AppActions.goToBack(path)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Item);
