import React, { PureComponent } from 'react';
import { 
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent, 
  IonCard, 
  IonCardContent, 
  IonCardHeader, 
  IonCardTitle, 
  IonLabel, 
  IonText, 
  IonList,
  IonItemDivider, 
  IonIcon,
  IonButton,
  IonButtons,
  IonFooter,
  IonGrid,
  IonRow,
  IonCol,
  IonItem,
  IonAvatar,
} from '@ionic/react';
import { connect } from 'react-redux';
import { add, arrowBack, informationCircleOutline, alertCircleOutline, locationOutline } from 'ionicons/icons';
import SlsOrdDetailActions from '../../Stores/SlsOrdDetail/Actions';
import AppActions from '../../Stores/App/Actions';
import OrderListItem from '../../components/ListItem/OrderListItem';
import { decimalScale } from '../../Services/Config';
import './CustomerDetail.css';
import { smallLogo } from '../../theme/image';

class CustomerDetail extends PureComponent {
  componentDidMount() {
    const {slsOrdDetailResetTimestamp, showHeader, showDetails, hdrId} = this.props;
    slsOrdDetailResetTimestamp();
    showHeader(hdrId);
    showDetails(hdrId);
  }

  render () {
    const { documentHeader, documentDetails, goToItem, goToBack } = this.props;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonButton shape="round" onClick={() => goToBack('customer')}><IonIcon icon={arrowBack} />BACK</IonButton>
            </IonButtons>
            <IonItem>
              <IonAvatar style={{"--border-radius":10}}>
                <img src={smallLogo} alt="small-logo"/>
              </IonAvatar>
              <IonTitle>
                Customer Details
              </IonTitle>
            </IonItem>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonCard key={documentHeader.id}>
            <IonCardContent>
              <IonCardHeader>
                <IonCardTitle><h1 className="cardText">{documentHeader.doc_code}</h1></IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonGrid>
                  <IonRow>
                    <IonCol size="auto"><IonIcon icon={alertCircleOutline}/></IonCol>
                    <IonCol><IonText class="cardText">{documentHeader.str_doc_status}</IonText></IonCol>
                  </IonRow>
                  {documentHeader.desc_01 && (
                    <IonRow>
                      <IonCol size="auto"><IonIcon icon={informationCircleOutline}/></IonCol>
                      <IonCol><IonText class="cardText">{documentHeader.desc_01}</IonText></IonCol>
                    </IonRow>
                  )}
                  {documentHeader.desc_02 && (
                    <IonRow>
                      <IonCol size="auto"><IonIcon icon={informationCircleOutline}/></IonCol>
                      <IonCol><IonText class="cardText">{documentHeader.desc_02}</IonText></IonCol>
                    </IonRow>
                  )}
                  {documentHeader.delivery_point && (
                    <IonRow>
                      <IonCol size="auto"><IonIcon icon={locationOutline}/></IonCol>
                      <IonCol>
                      {documentHeader.delivery_point.company_name_01 !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.company_name_01}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.unit_no !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.unit_no}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.building_name !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.building_name}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.street_name !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.street_name}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.district_01 !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.district_01}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.district_02 !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.district_02}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.postcode !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.postcode}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.state_name !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.state_name}</IonText>
                        </p>
                      )}
                      {documentHeader.delivery_point.country_name !== '' && (
                        <p>
                          <IonText class="cardText">{documentHeader.delivery_point.country_name}</IonText>
                        </p>
                      )}
                      </IonCol>
                  </IonRow>
                  )}
                </IonGrid>
              </IonCardContent>
            </IonCardContent>
          </IonCard>
          <IonList>
            <IonItemDivider sticky>
              <IonGrid>
                <IonRow>
                  <IonCol>
                    <h1>
                      {documentHeader.doc_code}
                    </h1>
                  </IonCol>
                  <IonCol class="ion-text-right ion-align-self-center">
                    <IonButton onClick={() => {goToItem()}}>
                      <IonIcon icon={add} />
                      <IonLabel>Add Order</IonLabel>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItemDivider>
            {documentDetails.map(documentDetail => {
              return (
                <OrderListItem key={documentDetail.id} item={documentDetail} />
              )
            })}
          </IonList>
          {/* <IonFab vertical="bottom" horizontal="end" slot="fixed">
            <IonButton onClick={() => {goToItem()}}>
              <IonIcon icon={add} />
              <IonLabel>Add Order</IonLabel>
            </IonButton>
        </IonFab> */}
        </IonContent>
        <IonFooter>
          <IonToolbar>
            <IonItem>
              <IonGrid>
                <IonRow>
                  <IonCol class="ion-align-self-center">
                    <h3>
                      Total:
                    </h3>
                  </IonCol>
                  <IonCol class="ion-align-self-center ion-text-right">
                    <h3>
                      {parseFloat(documentHeader.net_amt, decimalScale)}
                    </h3>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>
          </IonToolbar>
        </IonFooter>
      </IonPage>
    );
  }
}

const mapStateToProps = state => ({
  token: state.app.token,
  hdrId: state.outbOrd01.hdrId,
  documentHeader: state.slsOrdDetail.documentHeader,
  documentDetails: state.slsOrdDetail.documentDetails,
  appPath: state.app.appPath,
});

const mapDispatchToProps = dispatch => ({
  slsOrdDetailResetTimestamp: () => dispatch(SlsOrdDetailActions.slsOrdDetailResetTimestamp()),
  showHeader: hdrId => dispatch(SlsOrdDetailActions.slsOrdDetailShowHeader(hdrId)),
  showDetails: hdrId => dispatch(SlsOrdDetailActions.slsOrdDetailShowDetails(hdrId)),
  goToItem: () => dispatch(AppActions.goToItem()),
  goToBack: path => dispatch(AppActions.goToBack(path)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetail);