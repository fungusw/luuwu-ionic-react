import { put, call, select, takeLatest } from 'redux-saga/effects';
import { push, goBack } from 'connected-react-router';
import AppActions, { AppTypes } from '../Stores/App/Actions';
import ApiService from '../Services/ApiService';

const getAppStore = state => state.app;
// const getRouterStore = state => state.router;

export function* appAuthenticate({ formikBag, username, password, apiUrl }) {
    formikBag.setSubmitting(true);
    try {
      // const app = yield select(getAppStore);
      const postData = {
        username,
        password
      };
  
      const result = yield call(
        ApiService.postApi, // function
        apiUrl,
        // 'http://inverze.dyndns.org:8000/scm-backend/public/api/v1',
        'login/authenticate',
        '1035',
        postData // params
      );
      if (result.isSuccess === true) {
        console.log('resultSuccess', result.data);
        yield put(AppActions.appAuthenticateSuccess(result.data, result.message, apiUrl));
      } else if (result.isTokenExpired === true) {
        console.log('resultToken', result.message);
        yield put(AppActions.appTokenExpired(result.message));
      } else if (result.isPasswordExpired === true) {
        console.log('resultPassword', result.message);
        yield put(AppActions.appPasswordExpired(result.message));
      } else {
        console.log('resultUnknown', result.message);
        yield put(AppActions.setErrorMessage(result.message));
      }
    } catch (error) {
        console.log('resultError', error.message);
    } finally {
      formikBag.setSubmitting(false);
    }
  }
  
  export function* appAuthenticateSuccess({ successMessage }) {
    const app = yield select(getAppStore);
    yield put(push(`${app.appPath}/customer`));
  }

  export function* goToItem() {
    const app = yield select(getAppStore);
    yield put(push(`${app.appPath}/item`));
  }

  export function* goToBack({path}) {
    const app = yield select(getAppStore);
    yield put(push(`${app.appPath}/${path}`));
  }

  export function* appTokenExpired() {
    const app = yield select(getAppStore);
    yield put(push(`${app.appPath}/`));
  }

  export function* startUp() {
    // const app = yield select(getAppStore);
  
    // eslint-disable-next-line no-undef
    if (window.API_URL !== null || window.API_URL !== 'undefined' || window.API_URL !== '') {
      // update the API_URL from public/config.js
      // eslint-disable-next-line no-undef
      yield put(AppActions.appUpdateApiUrl(window.API_URL));
    }
  
    // eslint-disable-next-line no-undef
    if (window.APP_PATH !== null || window.APP_PATH !== 'undefined' || window.APP_PATH !== '') {
      // update the APP_PATH from public/config.js
      // eslint-disable-next-line no-undef
      yield put(AppActions.appUpdateAppPath(window.APP_PATH));
    }
  
    // yield put(replace(app.appPath + pathname));
    // if (app.token === null || app.token === '') {
    //   yield put(push(`${app.appPath}/login`));
    // }
  }

export function* goToProfile() {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/profile`));
}

export function* logout() {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/`));
}

export const saga = [
  takeLatest(AppTypes.APP_AUTHENTICATE, appAuthenticate),
  takeLatest(AppTypes.APP_AUTHENTICATE_SUCCESS, appAuthenticateSuccess),
  takeLatest(AppTypes.GO_TO_ITEM, goToItem),
  takeLatest(AppTypes.GO_TO_BACK, goToBack),
  takeLatest(AppTypes.START_UP, startUp),
  takeLatest(AppTypes.APP_TOKEN_EXPIRED, appTokenExpired),
  takeLatest(AppTypes.GO_TO_PROFILE, goToProfile),
  takeLatest(AppTypes.LOGOUT, logout),
];
