/* eslint-disable camelcase */
import {put, call, select, takeLatest} from 'redux-saga/effects';
// import i18next from 'i18next';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import SlsOrdDetailActions, {SlsOrdDetailTypes} from '../Stores/SlsOrdDetail/Actions';
import OutbOrd01Actions from '../Stores/OutbOrd01/Actions';
import UtilService from '../Services/UtilService';
import NavigationService from '../Services/NavigationService';
import ItemDetailsActions from '../Stores/ItemDetails/Actions';
// import PendingOrderActions from '../Stores/PendingOrder/Actions';

const getAppStore = state => state.app;

const getSlsOrdDetailStore = state => state.slsOrdDetail;

export function* slsOrdDetailInitHeader({divisionId}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `slsOrd/initHeader/${divisionId}`,
      app.token,
      getData,
      'multipart/form-data', // params
    );

    if (result.isSuccess === true) {
      yield put(SlsOrdDetailActions.slsOrdDetailShowHeaderSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailShowHeader({hdrId}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `slsOrd/showHeader/${hdrId}`,
      app.token,
      getData,
      'multipart/form-data', // params
    );

    if (result.isSuccess === true) {
      yield put(SlsOrdDetailActions.slsOrdDetailShowHeaderSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailTransitionToStatus({formikBag, hdrId, docStatus}) {
  formikBag.setSubmitting(true);
  yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));
  try {
    const app = yield select(getAppStore);

    const postData = {
      hdrId,
      docStatus,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      'slsOrd/transitionToStatus',
      app.token,
      postData, // params
    );

    if (result.isSuccess === true) {
      const slsOrdDetail = yield select(getSlsOrdDetailStore);

      const {documentHeader: oldDocumentHeader, documentDetails: oldDocumentDetails} = slsOrdDetail;

      const {
        document_header: retDocumentHeader,
        document_details: retDocumentDetails,
        deleted_details: retDeletedDetails,
      } = result.data;

      const processed = UtilService.processHeaderDetails(
        oldDocumentHeader,
        oldDocumentDetails,
        retDocumentHeader,
        retDocumentDetails,
        retDeletedDetails,
      );

      yield put(
        SlsOrdDetailActions.slsOrdDetailUpdateDocumentSuccess(retDocumentHeader, processed.details),
      );
    } else if (result.isTokenExpired === true) {
      formikBag.setSubmitting(false);
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      formikBag.setSubmitting(false);
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailUpdateHeader({formikBag, documentHeader}) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: documentHeader,
    };

    const result = yield call(
      ApiService.putApi, // function
      app.apiUrl,
      'slsOrd/updateHeader',
      app.token,
      postData, // params
    );

    if (result.isSuccess === true) {
      const slsOrdDetail = yield select(getSlsOrdDetailStore);

      const {documentHeader: oldDocumentHeader, documentDetails: oldDocumentDetails} = slsOrdDetail;

      const {
        document_header: retDocumentHeader,
        document_details: retDocumentDetails,
        deleted_details: retDeletedDetails,
      } = result.data;

      const processed = UtilService.processHeaderDetails(
        oldDocumentHeader,
        oldDocumentDetails,
        retDocumentHeader,
        retDocumentDetails,
        retDeletedDetails,
      );
      yield put(SlsOrdDetailActions.slsOrdDetailSetSuccessMessage(result.message));
      yield put(
        SlsOrdDetailActions.slsOrdDetailUpdateDocumentSuccess(retDocumentHeader, processed.details),
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailShowDetails({hdrId}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));

    let result = {
      isSuccess: true,
      data: [],
    };

    if (hdrId > 0) {
      const app = yield select(getAppStore);
      const getData = {};

      result = yield call(
        ApiService.getApi, // function
        app.apiUrl,
        `slsOrd/showDetails/${hdrId}`,
        app.token,
        getData,
        'multipart/form-data', // params
      );
    }

    if (result.isSuccess === true) {
      yield put(SlsOrdDetailActions.slsOrdDetailShowDetailsSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailUpdateDetails({formikBag, hdrId, documentDetails}) {
  formikBag.setSubmitting(true);
  yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));
  yield put(SlsOrdDetailActions.slsOrdDetailSetSuccessMessage(''));
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: documentDetails,
    };

    const result = yield call(
      ApiService.putApi, // function
      app.apiUrl,
      `slsOrd/updateDetails/${hdrId}`,
      app.token,
      postData, // params
    );

    if (result.isSuccess === true) {
      const slsOrdDetail = yield select(getSlsOrdDetailStore);

      const {documentHeader: oldDocumentHeader, documentDetails: oldDocumentDetails} = slsOrdDetail;

      const {
        document_header: retDocumentHeader,
        document_details: retDocumentDetails,
        deleted_details: retDeletedDetails,
      } = result.data;

      const processed = UtilService.processHeaderDetails(
        oldDocumentHeader,
        oldDocumentDetails,
        retDocumentHeader,
        retDocumentDetails,
        retDeletedDetails,
      );

      yield put(
        SlsOrdDetailActions.slsOrdDetailUpdateDocumentSuccess(retDocumentHeader, processed.details),
      );
      yield put(SlsOrdDetailActions.slsOrdDetailSetSuccessMessage(result.message));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailFetchSalesmanOptions({search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchSalesmanOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `user/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({value: d.id, label: `${d.username}`}));
      yield put(SlsOrdDetailActions.slsOrdDetailFetchSalesmanOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchSalesmanOptionLoading(false));
  }
}

export function* slsOrdDetailFetchDeliveryPointOptions({search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchDeliveryPointOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `deliveryPoint/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code} ${d.company_name_01}`,
      }));

      yield put(SlsOrdDetailActions.slsOrdDetailFetchDeliveryPointOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchDeliveryPointOptionLoading(false));
  }
}

export function* slsOrdDetailFetchCreditTermOptions({search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchCreditTermOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `creditTerm/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code}`,
      }));

      yield put(SlsOrdDetailActions.slsOrdDetailFetchCreditTermOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchCreditTermOptionLoading(false));
  }
}

export function* slsOrdDetailFetchCurrencyOptions({search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchCurrencyOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `currency/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code}`,
      }));

      yield put(SlsOrdDetailActions.slsOrdDetailFetchCurrencyOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchCurrencyOptionLoading(false));
  }
}

export function* slsOrdDetailChangeDeliveryPoint({formikBag, deliveryPointId}) {
  try {
    formikBag.setSubmitting(true);

    const app = yield select(getAppStore);
    const postData = {
      deliveryPointId,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `slsOrd/changeDeliveryPoint`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      Object.entries(result.data).forEach(entry => {
        const key = entry[0];
        const value = entry[1];
        formikBag.setFieldValue(key, value);
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailChangeCurrency({formikBag, currencyId}) {
  try {
    formikBag.setSubmitting(true);

    const app = yield select(getAppStore);
    const postData = {
      currencyId,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `slsOrd/changeCurrency`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      Object.entries(result.data).forEach(entry => {
        const key = entry[0];
        const value = entry[1];
        formikBag.setFieldValue(key, value);
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailCreateHeader({formikBag, documentHeader}) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: documentHeader,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      'slsOrd/createHeader',
      app.token,
      postData, // params
    );

    if (result.isSuccess === true) {
      yield put(SlsOrdDetailActions.slsOrdDetailCreateHeaderSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailCreateHeaderSuccess({hdrId}) {
  try {
    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `slsOrd/showHeader/${hdrId}`,
      app.token,
      getData,
      'multipart/form-data', // params
    );

    if (result.isSuccess === true) {
      yield put(OutbOrd01Actions.outbOrd01ChooseHeader(result.data.id, result.data.doc_code));
      NavigationService.goBack();
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  }
}

export function* slsOrdDetailFetchItemOptions({search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchItemOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `item/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code} ${d.desc_01}`,
      }));

      yield put(SlsOrdDetailActions.slsOrdDetailFetchItemOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchItemOptionLoading(false));
  }
}

export function* slsOrdDetailChangeItem({formikBag, hdrId, itemId}) {
  try {
    formikBag.setSubmitting(true);

    const app = yield select(getAppStore);
    const postData = {
      hdrId,
      itemId,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `slsOrd/changeItem`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      Object.entries(result.data).forEach(entry => {
        const key = entry[0];
        const value = entry[1];
        formikBag.setFieldValue(key, value);
      });

      // reset the select2 cache
      formikBag.setFieldValue('uom_select2', {
        value: 0,
        label: '',
      });
      formikBag.setFieldValue('uom_rate', 1);

      yield put(SlsOrdDetailActions.slsOrdDetailFetchUomOptionSuccess([]));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailChangeUom({formikBag, hdrId, itemId, uomId}) {
  try {
    // formikBag.setSubmitting(true);

    const app = yield select(getAppStore);
    const postData = {
      hdrId,
      itemId,
      uomId,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `slsOrd/changeItemUom`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      Object.entries(result.data).forEach(entry => {
        const key = entry[0];
        const value = entry[1];
        formikBag.setFieldValue(key, value);
      });
      formikBag.setFieldValue('item_select2', {
        value: itemId,
        label: '',
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    // formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailFetchUomOptions({itemId, search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchUomOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [
        {
          field: 'item_id',
          value: itemId,
        },
      ],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `itemUom/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.uom_id,
        label: `${d.uom_code}`,
      }));
      yield put(SlsOrdDetailActions.slsOrdDetailFetchUomOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchUomOptionLoading(false));
  }
}

export function* slsOrdDetailCreateDetail({formikBag, hdrId, documentDetail, pendingIndex}) {
  formikBag.setSubmitting(true);
  yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));
  // yield put(SlsOrdDetailActions.slsOrdDetailSetSuccessMessage(''));
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: documentDetail,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `slsOrd/createDetail/${hdrId}`,
      app.token,
      postData, // params
    );

    if (result.isSuccess === true) {
      const slsOrdDetail = yield select(getSlsOrdDetailStore);

      const {documentHeader: oldDocumentHeader, documentDetails: oldDocumentDetails} = slsOrdDetail;

      const {
        document_header: retDocumentHeader,
        document_details: retDocumentDetails,
        deleted_details: retDeletedDetails,
      } = result.data;

      const processed = UtilService.processHeaderDetails(
        oldDocumentHeader,
        oldDocumentDetails,
        retDocumentHeader,
        retDocumentDetails,
        retDeletedDetails,
      );

      yield put(
        SlsOrdDetailActions.slsOrdDetailUpdateDocumentSuccess(retDocumentHeader, processed.details),
      );

      yield put(
        SlsOrdDetailActions.slsOrdDetailSetInitDocumentDetail(
          slsOrdDetail.initDocumentDetail,
          documentDetail.item_id,
          documentDetail.desc_01,
        ),
      );

      if (pendingIndex || pendingIndex >= 0) {
        // yield put(PendingOrderActions.removePendingOrder(pendingIndex));
      }
      yield put(ItemDetailsActions.showSuccessMessage('Order Added'));
      // yield put(SlsOrdDetailActions.slsOrdDetailSetSuccessMessage(result.message));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailDeleteDetail({hdrId, documentDetail}) {
  yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(true));
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: [{id: documentDetail.id}],
    };

    const result = yield call(
      ApiService.deleteApi, // function
      app.apiUrl,
      `slsOrd/deleteDetails/${hdrId}`,
      app.token,
      postData, // params
    );

    if (result.isSuccess === true) {
      const slsOrdDetail = yield select(getSlsOrdDetailStore);

      const {documentHeader: oldDocumentHeader, documentDetails: oldDocumentDetails} = slsOrdDetail;

      const {
        document_header: retDocumentHeader,
        document_details: retDocumentDetails,
        deleted_details: retDeletedDetails,
      } = result.data;

      const processed = UtilService.processHeaderDetails(
        oldDocumentHeader,
        oldDocumentDetails,
        retDocumentHeader,
        retDocumentDetails,
        retDeletedDetails,
      );

      yield put(
        SlsOrdDetailActions.slsOrdDetailUpdateDocumentSuccess(retDocumentHeader, processed.details),
      );

      //   yield call(notification.success, {
      //     message: result.message,
      //     duration: parseInt(process.env.REACT_APP_SUCCESS_MESSAGE_DURATION, 10),
      //   });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailShowDocumentLoading(false));
  }
}

export function* slsOrdDetailFetchLocationOptions({siteFlowId, search}) {
  try {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchLocationOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      siteFlowId,
      search,
      filters: [],
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `location/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code} ${d.desc_01}`,
      }));

      yield put(SlsOrdDetailActions.slsOrdDetailFetchLocationOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
      //   yield call(notification.error, {
      //     message: result.message,
      //     duration: parseInt(process.env.REACT_APP_ERROR_MESSAGE_DURATION, 10),
      //   });
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
    // yield call(notification.error, {
    //   message: error.message,
    //   duration: parseInt(process.env.REACT_APP_ERROR_MESSAGE_DURATION, 10),
    // });
  } finally {
    yield put(SlsOrdDetailActions.slsOrdDetailFetchLocationOptionLoading(false));
  }
}

/* set item to get the item id and desc_01 desc_02 */
export function* slsOrdDetailSetItem({hdrId, itemId}) {
  try {
    const app = yield select(getAppStore);
    const postData = {
      hdrId,
      itemId,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `slsOrd/changeItem`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      // Object.entries(result.data).forEach(entry => {
      //   const key = entry[0];
      //   const value = entry[1];
      //   formikBag.setFieldValue(key, value);
      // });
      // // reset the select2 cache
      // formikBag.setFieldValue('uom_select2', {
      //   value: 0,
      //   label: '',
      // });
      // formikBag.setFieldValue('uom_rate', 1);
      yield put(SlsOrdDetailActions.slsOrdDetailSetItemSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
      //   yield call(notification.error, {
      //     message: result.message,
      //     duration: parseInt(process.env.REACT_APP_ERROR_MESSAGE_DURATION, 10),
      //   });
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
    // yield call(notification.error, {
    //   message: error.message,
    //   duration: parseInt(process.env.REACT_APP_ERROR_MESSAGE_DURATION, 10),
    // });
  } finally {
    // formikBag.setSubmitting(false);
  }
}

export function* slsOrdDetailFetchAllLocationOptions({siteFlowId, search, page}) {
  try {
    // yield put(SlsOrdDetailActions.slsOrdDetailFetchLocationOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      siteFlowId,
      search,
      filters: [],
      page,
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `location/select2`,
      app.token,
      postData,
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code} ${d.desc_01}`,
      }));
      yield put(SlsOrdDetailActions.slsOrdDetailFetchAllLocationOptionSuccess(options));
      if (page < result.data.last_page) {
        yield put(
          SlsOrdDetailActions.slsOrdDetailFetchAllLocationOptions(siteFlowId, search, page + 1),
        );
      }
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(SlsOrdDetailActions.slsOrdDetailSetErrorMessage(error.message));
  } finally {
    // yield put(SlsOrdDetailActions.slsOrdDetailFetchLocationOptionLoading(false));
  }
}

export function* searchLocationOptionOffline() {
  const slsOrdDetail = yield select(getSlsOrdDetailStore);
  const {cacheLocationsOption} = slsOrdDetail;
  yield put(SlsOrdDetailActions.slsOrdDetailFetchLocationOptionSuccess(cacheLocationsOption));
}

export function* changeUomOffline({formikBag, uomId}) {
  const slsOrdDetail = yield select(getSlsOrdDetailStore);
  const {uomOptions} = slsOrdDetail;
  const index = uomOptions
    .map(e => {
      return e.value;
    })
    .indexOf(uomId);
  formikBag.setFieldValue('uom_rate', uomOptions[index].uomRate);
}

export const saga = [
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_INIT_HEADER, slsOrdDetailInitHeader),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_SHOW_HEADER, slsOrdDetailShowHeader),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_UPDATE_HEADER, slsOrdDetailUpdateHeader),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_CREATE_HEADER, slsOrdDetailCreateHeader),
  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_CREATE_HEADER_SUCCESS,
    slsOrdDetailCreateHeaderSuccess,
  ),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_TRANSITION_TO_STATUS, slsOrdDetailTransitionToStatus),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_SHOW_DETAILS, slsOrdDetailShowDetails),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_UPDATE_DETAILS, slsOrdDetailUpdateDetails),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_CREATE_DETAIL, slsOrdDetailCreateDetail),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_DELETE_DETAIL, slsOrdDetailDeleteDetail),

  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_SALESMAN_OPTIONS,
    slsOrdDetailFetchSalesmanOptions,
  ),

  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_DELIVERY_POINT_OPTIONS,
    slsOrdDetailFetchDeliveryPointOptions,
  ),
  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_CHANGE_DELIVERY_POINT,
    slsOrdDetailChangeDeliveryPoint,
  ),

  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_CREDIT_TERM_OPTIONS,
    slsOrdDetailFetchCreditTermOptions,
  ),

  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_CURRENCY_OPTIONS,
    slsOrdDetailFetchCurrencyOptions,
  ),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_CHANGE_CURRENCY, slsOrdDetailChangeCurrency),

  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_LOCATION_OPTIONS,
    slsOrdDetailFetchLocationOptions,
  ),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_ITEM_OPTIONS, slsOrdDetailFetchItemOptions),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_CHANGE_ITEM, slsOrdDetailChangeItem),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_CHANGE_UOM, slsOrdDetailChangeUom),
  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_UOM_OPTIONS, slsOrdDetailFetchUomOptions),

  takeLatest(SlsOrdDetailTypes.SLS_ORD_DETAIL_SET_ITEM, slsOrdDetailSetItem),

  takeLatest(
    SlsOrdDetailTypes.SLS_ORD_DETAIL_FETCH_ALL_LOCATION_OPTIONS,
    slsOrdDetailFetchAllLocationOptions,
  ),
  takeLatest(SlsOrdDetailTypes.SEARCH_LOCATION_OPTION_OFFLINE, searchLocationOptionOffline),
  takeLatest(SlsOrdDetailTypes.CHANGE_UOM_OFFLINE, changeUomOffline),
];
