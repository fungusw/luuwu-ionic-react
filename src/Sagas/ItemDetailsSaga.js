import { push, goBack } from 'connected-react-router';
import {put, call, select, takeLatest} from 'redux-saga/effects';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
// import CatalogActions from '../Stores/Catalog/Actions';
import ItemDetailsActions, {ItemDetailsTypes} from '../Stores/ItemDetails/Actions';
// import NavigationService from '../Services/NavigationService';
// import ItemUpdateActions from '../Stores/ItemUpdate/Actions';

const getAppStore = state => state.app;

export function* fetchItemPhotos({itemId, page}) {
  try {
    yield put(ItemDetailsActions.fetchItemPhotosLoading(true));

    const app = yield select(getAppStore);
    const getData = {
      page,
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `item/showPhotos/${itemId}`,
      app.token,
      getData,
      'multipart/form-data', // params
    );
    if (result.isSuccess === true) {
      // if currentPage is more than lastPage, then currentPage = lastPage
      let currentPage = result.data.current_page;
      if (currentPage > result.data.last_page) {
        currentPage = result.data.last_page;
      }
      yield put(
        ItemDetailsActions.fetchItemPhotosSuccess(
          result.data.data,
          currentPage,
          result.data.last_page,
          result.data.total,
          '',
        ),
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(ItemDetailsActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  } finally {
    yield put(ItemDetailsActions.fetchItemPhotosLoading(false));
  }
}

export function* selectItem() {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/itemDetail`));
}

export function* selectPhotos() {
  // yield put(push(''));
}

// export function* uploadItemPhoto({
//   formikBag,
//   itemPhotoIndex,
//   itemId,
//   isFeatured,
//   uomId,
//   desc01,
//   desc02,
//   photo,
// }) {
//   formikBag.setSubmitting(true);

//   const pathTokens = photo.path.split('/');
//   let filename = '';
//   if (pathTokens.length > 0) {
//     filename = pathTokens[pathTokens.length - 1];
//   }
//   try {
//     const app = yield select(getAppStore);

//     // FormData is a global component and therefore must be exempted to avoid eslint warnings
//     /* global FormData:false */
//     const postData = new FormData();
//     postData.append('file', {
//       name: filename,
//       // uri: Platform.OS === 'android' ? photo.path : photo.path.replace('file://', ''),
//       type: photo.mime,
//     });
//     postData.append('filename', filename);
//     postData.append('isFeatured', isFeatured);
//     postData.append('uom_id', uomId);
//     postData.append('desc_01', desc01);
//     postData.append('desc_02', desc02);

//     const getData = {};

//     const result = yield call(
//       ApiService.postApi, // function
//       app.apiUrl,
//       `item/createPhoto/${itemId}`,
//       app.token,
//       postData,
//       getData,
//       'multipart/form-data', // params
//     );
//     if (result.isSuccess === true) {
//       formikBag.setSubmitting(false);
//       yield put(
//         ItemDetailsActions.uploadItemPhotoSuccess(
//           itemPhotoIndex,
//           result.data.data.item,
//           result.data.data.itemPhotos,
//         ),
//       );
//       // yield put(ItemUpdateActions.ItemUpdateUploadItemPhotoSuccess());
//     } else if (result.isTokenExpired === true) {
//       formikBag.setSubmitting(false);
//       yield put(AppActions.tokenExpired(result.message));
//     } else if (result.isPasswordExpired === true) {
//       formikBag.setSubmitting(false);
//       yield put(AppActions.passwordExpired(result.message));
//     } else {
//       formikBag.setSubmitting(false);
//       yield put(ItemDetailsActions.showErrorMessage(result.message));
//     }
//   } catch (error) {
//     formikBag.setSubmitting(false);
//     yield put(ItemDetailsActions.showErrorMessage(error.message));
//   } finally {
//     // formikBag.setSubmitting(false);
//   }
// }

export function* uploadItemPhotoSuccess({item}) {
  try {
    // yield put(CatalogActions.updateItemPhotoSuccess(item));
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  }
}

export function* stopUploadItemPhoto() {
  try {
    yield put(goBack());
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  }
}

export function* goToItemUpdate({itemId}) {
  // yield put(ItemUpdateActions.setItemId(itemId));
  // yield put(push(''));
}

export const saga = [
  takeLatest(ItemDetailsTypes.FETCH_ITEM_PHOTOS, fetchItemPhotos),
  takeLatest(ItemDetailsTypes.SELECT_ITEM, selectItem),
  takeLatest(ItemDetailsTypes.SELECT_PHOTOS, selectPhotos),
  // takeLatest(ItemDetailsTypes.UPLOAD_ITEM_PHOTO, uploadItemPhoto),
  takeLatest(ItemDetailsTypes.UPLOAD_ITEM_PHOTO_SUCCESS, uploadItemPhotoSuccess),
  takeLatest(ItemDetailsTypes.STOP_UPLOAD_ITEM_PHOTO, stopUploadItemPhoto),
  takeLatest(ItemDetailsTypes.GO_TO_ITEM_UPDATE, goToItemUpdate),
];
