import { put, call, select, takeLatest } from 'redux-saga/effects';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import ItemActions, { ItemTypes } from '../Stores/Item/Actions';

const getAppStore = state => state.app;

export function* fetchItems({ page, sorts, filters, e }) {
  try {
    yield put(ItemActions.fetchItemsLoading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(([key, value]) => {
      processedSorts.push(`${key}:${value > 0 ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(([key, value]) => {
      if (key === 'item_group01_code_in') {
        // it is array
        let valueList = '';
        value.forEach(a => {
          if (valueList.length === 0) {
            valueList += a;
          } else {
            valueList = `${valueList},${a}`;
          }
        });
        if (valueList) {
          processedFilters.push(`${key}:${valueList}`);
        }
      } else if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page,
      sorts: processedSorts,
      filters: processedFilters,
      pageSize: '20'
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `item/indexProcess/ITEM_LIST_02`,
      app.token,
      getData // params
    );
    if (result.isSuccess === true) {
      // if currentPage is more than lastPage, then currentPage = lastPage
      let currentPage = result.data.current_page;
      if (currentPage > result.data.last_page) {
        currentPage = result.data.last_page;
      }
      yield put(
        ItemActions.fetchItemsSuccess(
          result.data.data,
          currentPage,
          result.data.last_page,
          result.data.total,
          ''
        )
      );
    } else if (result.isTokenExpired === true) {
      console.log('resultToken', result);
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(ItemActions.appPasswordExpired(result.message));
    }
  } catch (error) {
    yield put(ItemActions.showErrorMessage(error.message));
  } finally {
    if(e){
      e.target.complete();
    }
    yield put(ItemActions.fetchItemsLoading(false));
  }
}

export const saga = [
  takeLatest(ItemTypes.FETCH_ITEMS, fetchItems),
];
