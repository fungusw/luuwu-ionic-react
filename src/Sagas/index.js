import { all } from 'redux-saga/effects';
import { saga as AppSaga } from './AppSaga';
import { saga as ItemSaga } from './ItemSaga';
import { saga as OutbOrd01Saga } from './OutbOrd01Saga';
import { saga as SlsOrdDetailSaga } from './SlsOrdDetailSaga';
import { saga as ItemDetailsSaga } from './ItemDetailsSaga';

export default function* root() {
    yield all([
      /**
       * @see https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
       */
      ...AppSaga,
      ...ItemSaga,
      ...OutbOrd01Saga,
      ...SlsOrdDetailSaga,
      ...ItemDetailsSaga,
    ]);
  }
  