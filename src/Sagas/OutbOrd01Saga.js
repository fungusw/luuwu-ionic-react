/* eslint-disable camelcase */
import {put, call, select, takeLatest} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import OutbOrd01Actions, {OutbOrd01Types} from '../Stores/OutbOrd01/Actions';

const getAppStore = state => state.app;

export function* outbOrd01FetchOutbOrd01({divisionId, currentPage, sorts, filters, pageSize, e}) {
  try {
    yield put(OutbOrd01Actions.outbOrd01FetchOutbOrd01Loading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      pageSize,
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      // `outbOrd/indexProcess/OUTB_ORD_01/1`,
      `outbOrd/indexProcess/OUTB_ORD_01/${divisionId}`,
      app.token,
      getData,
      'multipart/form-data', // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        OutbOrd01Actions.outbOrd01FetchOutbOrd01Success(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page,
        ),
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield put(OutbOrd01Actions.outbOrd01SetErrorMessage(result.message));
    }
  } catch (error) {
    yield put(OutbOrd01Actions.outbOrd01SetErrorMessage(error.message));
  } finally {
    if (e) {
      e.target.complete();
    }
    yield put(OutbOrd01Actions.outbOrd01FetchOutbOrd01Loading(false));
  }
}

export function* outbOrd01GoToCustomer() {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/customerDetail`));
}

export const saga = [
  takeLatest(OutbOrd01Types.OUTB_ORD01_FETCH_OUTB_ORD01, outbOrd01FetchOutbOrd01),
  takeLatest(OutbOrd01Types.OUTB_ORD01_GO_TO_CUSTOMER, outbOrd01GoToCustomer),
];
