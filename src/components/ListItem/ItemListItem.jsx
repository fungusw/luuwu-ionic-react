import React, { PureComponent } from 'react';
import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonThumbnail, IonItem } from '@ionic/react';
import { connect } from 'react-redux';
import axios from 'axios';
import { noPhotoMedium } from '../../theme/image';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';

class ItemListItem extends PureComponent {
  constructor() {
    super();

    this.fetchPhoto = this.fetchPhoto.bind(this);
    this.selectItem = this.selectItem.bind(this);
  }

  componentDidMount() {
    const { item } = this.props;
    if (item.feat_icon_url && item.feat_icon_url !== '') {
      console.log(item.feat_icon_url);
      this.fetchPhoto(item.feat_icon_url, item);
    }
  }

  async fetchPhoto(url, item) {
    const { token } = this.props;
    try {
      await axios({
        method: 'get',
        url,
        headers: {
          Accept: '*/*',
          'Access-Control-Allow-Origin': '*',
          Authorization: `Bearer ${token}`,
        },
        responseType: 'blob',
      }).then(response => {
        var reader = new window.FileReader();
        reader.readAsDataURL(response.data); 
        reader.onload = function() {
            var imageDataUrl = reader.result;
            if(document.getElementById(`item${item.id}`)){
              document.getElementById(`item${item.id}`).setAttribute('src', imageDataUrl);
            }
        }
      });
    } catch (error) {
      console.log('catchError:', error);
    }
  }

  selectItem(itemId, desc_01, desc02) {
    const {selectItem} = this.props;
    selectItem(itemId, desc_01, desc02);
  }

  render () {
    const { item } = this.props;
    return (
      <IonCard key={item.id} button onClick={() => {this.selectItem(item.id, item.desc_01, item.desc_02)}}>
        <IonItem>
          <IonThumbnail slot="start"><img id={`item${item.id}`} src={noPhotoMedium} alt={item.desc_01}/></IonThumbnail>
          <div>
            <IonCardHeader><IonCardTitle>{item.desc_01}</IonCardTitle></IonCardHeader>
            <IonCardContent>{item.code}</IonCardContent>
          </div>
        </IonItem>
      </IonCard>
    );
  }
}

const mapStateToProps = state => ({
  token: state.app.token,
});

const mapDispatchToProps = dispatch => ({
  selectItem: (itemId, itemDesc01, itemDesc02) => dispatch(ItemDetailsActions.selectItem(itemId, itemDesc01, itemDesc02)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemListItem);