import React, { PureComponent } from 'react';
import { IonCard, IonThumbnail, IonGrid, IonRow, IonCol, IonIcon, IonText } from '@ionic/react';
import { connect } from 'react-redux';
import OutbOrd01Actions from '../../Stores/OutbOrd01/Actions';
import {decimalScale} from '../../Services/Config';
import { noPhotoMedium } from '../../theme/image';
import { chevronDown, chevronUp } from 'ionicons/icons';

class OrderListItem extends PureComponent {
  constructor() {
    super();

    this.state = {
      show: false,
    };
  }
  componentDidMount() {}

  render () {
    const { show } = this.state;
    const { item } = this.props;
    const total = parseFloat(item.sale_price).toFixed(decimalScale) * parseFloat(item.qty).toFixed(decimalScale);
    const total_price_disc = parseFloat(item.price_disc).toFixed(decimalScale) * parseFloat(item.qty).toFixed(decimalScale);
    return (
      <IonCard button onClick={() => this.setState({show: !show})} color="">
        <IonThumbnail slot="start">
          <img src={noPhotoMedium} alt="img"/>
        </IonThumbnail>
        <IonGrid>
          <IonRow>
            <IonCol>
              <h2>
                {item.desc_01}
              </h2>
            </IonCol>
            {!show && (
              <IonCol class="ion-text-right ion-align-self-center" size="auto">
                <IonText color="success">
                  <h5>
                    {parseFloat(item.net_amt).toFixed(decimalScale)}
                  </h5>
                </IonText>
              </IonCol>
            )}
            {show ? (
              <IonCol class="ion-align-self-center ion-text-right" size="1">
                <IonIcon icon={chevronUp} />
              </IonCol>
            ):(
              <IonCol class="ion-align-self-center ion-text-right" size="1">
                <IonIcon icon={chevronDown} />
              </IonCol>
            )}
          </IonRow>
          {show && (
            <>
            <IonRow>
              <IonCol>
                {parseFloat(item.sale_price).toFixed(decimalScale)} X {parseInt(item.qty, 10)}
              </IonCol>
              <IonCol class="">
                
              </IonCol>
              <IonCol class="ion-text-right">
                {parseFloat(total).toFixed(decimalScale)}
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                
              </IonCol>
              <IonCol class="ion-text-right">
                {`Discount(-${parseFloat(total_price_disc).toFixed(decimalScale)})`}
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                
              </IonCol>
              <IonCol class="ion-text-right">
                {`Tax Amt(-${parseFloat(item.dtl_disc_amt).toFixed(decimalScale)})`}
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol class="ion-text-right">
                <IonText color="success">
                  <h5>
                    {parseFloat(item.net_amt).toFixed(decimalScale)}
                  </h5>
                </IonText>
              </IonCol>
            </IonRow>
            </>
          )}
        </IonGrid>
      </IonCard>
    );
  }
}

const mapStateToProps = state => ({
  token: state.app.token,
});

const mapDispatchToProps = dispatch => ({
  goToCustomer: hdrId => dispatch(OutbOrd01Actions.outbOrd01GoToCustomer(hdrId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderListItem);