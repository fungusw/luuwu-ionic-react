import React, { PureComponent } from 'react';
import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonIcon } from '@ionic/react';
import { connect } from 'react-redux';
import OutbOrd01Actions from '../../Stores/OutbOrd01/Actions';
import { pinOutline, helpCircleOutline } from 'ionicons/icons';

class ItemListItem extends PureComponent {
  componentDidMount() {}

  render () {
    const { item, goToCustomer } = this.props;
    return (
      <IonCard button onClick={() => goToCustomer(item.id)} key={item.id}>
        <IonCardContent>
          {/* <img id={`item${item.id}`} src={noPhotoMedium} alt={item.desc_01}/> */}
          <IonCardHeader><IonCardTitle>{item.doc_code}</IonCardTitle></IonCardHeader>
          <IonCardContent>
            <p><IonIcon icon={pinOutline} />{item.delivery_point_company_name_01}</p>
            <p><IonIcon icon={helpCircleOutline} />{item.str_doc_status}</p>
          </IonCardContent>
        </IonCardContent>
      </IonCard>
    );
  }
}

const mapStateToProps = state => ({
  token: state.app.token,
});

const mapDispatchToProps = dispatch => ({
  goToCustomer: hdrId => dispatch(OutbOrd01Actions.outbOrd01GoToCustomer(hdrId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemListItem);