import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  resetItems: null,
  fetchItems: ['page', 'sorts', 'filters', 'pageSize', 'e'],
  fetchItemsLoading: ['boolean'],
  fetchItemsSuccess: ['items', 'currentPage', 'lastPage', 'total', 'successMessage'],

  resetItemGroup01s: null,

  setFilters: ['filters'],
  resetFilters: null,

  showErrorMessage: ['errorMessage'],
  goToFusion : null,
});

export const ItemTypes = Types;
export default Creators;
