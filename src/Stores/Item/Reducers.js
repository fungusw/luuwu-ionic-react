/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import INITIAL_STATE from './InitialState';
import { ItemTypes } from './Actions';

export const resetItems = state => ({
  ...state,
  items: []
});

export const fetchItemsLoading = (state, { boolean }) => ({
  ...state,
  itemsIsLoading: boolean
});

export const fetchItemsSuccess = (
  state,
  { items, currentPage, lastPage, total, successMessage }
) => ({
  ...state,
  items: [...state.items, ...items],
  currentPage,
  lastPage,
  total,
  successMessage,
  errorMessage: ''
});

export const updateItemPhotoSuccess = (state, { item }) => ({
  ...state,
  items: state.items.map(oldItem => {
    if (oldItem.id === item.id) {
      return {
        ...oldItem,
        feat_icon_url: item.feat_icon_url
      };
    }
    return oldItem;
  })
});

export const setFilters = (state, { filters }) => ({
  ...state,
  filters: {
    ...state.filters,
    ...filters
  }
});

export const resetFilters = state => ({
  ...state,
  filters: INITIAL_STATE.filters
});

export const showErrorMessage = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemTypes.RESET_ITEMS]: resetItems,
  [ItemTypes.FETCH_ITEMS_LOADING]: fetchItemsLoading,
  [ItemTypes.FETCH_ITEMS_SUCCESS]: fetchItemsSuccess,

  [ItemTypes.SET_FILTERS]: setFilters,
  [ItemTypes.RESET_FILTERS]: resetFilters,

  [ItemTypes.SHOW_ERROR_MESSAGE]: showErrorMessage
});
