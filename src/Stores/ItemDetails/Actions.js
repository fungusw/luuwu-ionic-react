import {createActions} from 'reduxsauce';

const {Types, Creators} = createActions({
  selectItem: ['itemId', 'itemDesc01', 'itemDesc02'],
  resetItemPhotos: null,
  fetchItemPhotos: ['itemId', 'page', 'sorts', 'filters', 'pageSize'],
  fetchItemPhotosLoading: ['boolean'],
  fetchItemPhotosSuccess: ['itemPhotos', 'currentPage', 'lastPage', 'total', 'successMessage'],
  selectPhotos: ['photos'],
  uploadItemPhoto: [
    'formikBag',
    'itemPhotoIndex',
    'itemId',
    'isFeatured',
    'uomId',
    'desc01',
    'desc02',
    'photo',
  ],
  uploadItemPhotoSuccess: ['itemPhotoIndex', 'item', 'itemPhotos'],
  stopUploadItemPhoto: null,
  showErrorMessage: ['errorMessage'],
  showSuccessMessage: ['successMessage'],
  goToItemUpdate: ['itemId'],
  setShowSuccessToast: ['showSuccessToast'],
  setShowErrorToast: ['showErrorToast'],
});

export const ItemDetailsTypes = Types;
export default Creators;
