/**
 * The initial values for the redux state.
 */
export default {
  itemId: 0,
  itemPhotos: [],
  itemPhotosIsLoading: false,
  itemPhotosSorts: {},
  itemPhotosFilters: {},
  itemPhotosPageSize: 20,
  itemPhotosCurrentPage: 1,
  itemPhotosLastPage: 10,
  itemPhotosTotal: 100,
  selectedPhotos: [],
  successMessage: '',
  errorMessage: '',
  /* itemDesc01 = item.desc_01 for header text in itemDetailsScreen  */
  itemDesc01: '',
  /* itemDesc = item.desc_02 to replace slsOrdDetailSetItem */
  itemDesc02: '',
  showSuccessToast: false,
  showErrorToast: false,
};
