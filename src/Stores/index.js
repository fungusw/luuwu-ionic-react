// eslint-disable-next-line import/no-extraneous-dependencies
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import configureStore from './CreateStore';
import rootSaga from '../Sagas';
import { reducer as AppReducer } from './App/Reducers';
import { reducer as ItemReducer } from './Item/Reducers';
import { reducer as OutbOrd01Reducer } from './OutbOrd01/Reducers';
import { reducer as SlsOrdDetailReducer } from './SlsOrdDetail/Reducers';
import { reducer as ItemDetailsReducer } from './ItemDetails/Reducers';

export default () => {
  const rootReducer = history =>
    combineReducers({
      router: connectRouter(history),
      /**
       * Register your reducers here.
       * @see https://redux.js.org/api-reference/combinereducers
       */
      app: AppReducer,
      item: ItemReducer,
      outbOrd01: OutbOrd01Reducer,
      slsOrdDetail: SlsOrdDetailReducer,
      itemDetails: ItemDetailsReducer,
    });

  return configureStore(rootReducer, rootSaga);
};
