import {createActions} from 'reduxsauce';

const {Types, Creators} = createActions({
  outbOrd01ResetTimestamp: ['currentPage', 'sorts', 'filters', 'pageSize'],
  // outbOrd01GoToDocument: ['hdrId'],
  // outbOrd01NewDocument: ['divisionId'],
  outbOrd01FetchOutbOrd01: ['divisionId', 'currentPage', 'sorts', 'filters', 'pageSize', 'e'],
  outbOrd01FetchOutbOrd01Loading: ['boolean'],
  outbOrd01FetchOutbOrd01Success: ['documents', 'currentPage', 'lastPage', 'total', 'pageSize'],
  // outbOrd01AddSelectedDocuments: ['selectedDocuments'],
  // outbOrd01RemoveSelectedDocuments: ['selectedDocuments'],
  // outbOrd01SetWorkspaceVisible: ['boolean'],
  // outbOrd01CreateOutbOrd01: ['hdrIds'],
  // outbOrd01CreateOutbOrd01Loading: ['boolean'],
  // outbOrd01CreateOutbOrd01Success: ['newDocuments'],
  // outbOrd01CheckStock: ['hdrIds'],

  // outbOrd01SetExpandedRows: ['expandedRows'],
  // outbOrd01GoToHeader: null,
  outbOrd01GoToCustomer: ['hdrId'],
  // outbOrd01ResetHdrId: null,
  outbOrd01SetErrorMessage: ['errorMessage'],
  outbOrd01SetFilters: ['filters'],
  // outbOrd01SetHeader: ['hdrId', 'doc_code'],
  // outbOrd01ChooseHeader: ['hdrId', 'doc_code'],
  // outbOrd01GoToAddHeader: null,
  // outbOrd01ViewOrder: ['hdrId', 'doc_code'],
});

export const OutbOrd01Types = Types;
export default Creators;
