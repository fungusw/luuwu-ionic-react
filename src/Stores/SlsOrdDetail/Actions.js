import {createActions} from 'reduxsauce';

const {Types, Creators} = createActions({
  slsOrdDetailResetTimestamp: null,
  slsOrdDetailSetHdrId: ['hdrId', 'itemId'],

  slsOrdDetailInitHeader: ['divisionId'],
  slsOrdDetailShowHeader: ['hdrId'],
  slsOrdDetailShowHeaderSuccess: ['documentHeader'],

  slsOrdDetailUpdateHeader: ['formikBag', 'documentHeader'],

  slsOrdDetailCreateHeader: ['formikBag', 'documentHeader'],
  slsOrdDetailCreateHeaderSuccess: ['hdrId'],
  slsOrdDetailUpdateDocumentSuccess: ['documentHeader', 'documentDetails'],

  slsOrdDetailShowDocumentLoading: ['boolean'],
  slsOrdDetailTransitionToStatus: ['formikBag', 'hdrId', 'docStatus'],

  slsOrdDetailShowDetails: ['hdrId'],
  slsOrdDetailShowDetailsSuccess: ['documentDetails'],

  slsOrdDetailSetDetailVisible: ['boolean'],
  slsOrdDetailSetDocumentDetail: ['documentDetail'],

  slsOrdDetailUpdateDetails: ['formikBag', 'hdrId', 'documentDetails'],
  /* added pendingIndex to remove pending order */
  slsOrdDetailCreateDetail: ['formikBag', 'hdrId', 'documentDetail', 'pendingIndex'],
  slsOrdDetailDeleteDetail: ['hdrId', 'documentDetail'],

  slsOrdDetailFetchSalesmanOptions: ['search'],
  slsOrdDetailFetchSalesmanOptionLoading: ['boolean'],
  slsOrdDetailFetchSalesmanOptionSuccess: ['options'],

  slsOrdDetailFetchDeliveryPointOptions: ['search'],
  slsOrdDetailFetchDeliveryPointOptionLoading: ['boolean'],
  slsOrdDetailFetchDeliveryPointOptionSuccess: ['options'],
  slsOrdDetailChangeDeliveryPoint: ['formikBag', 'deliveryPointId'],

  slsOrdDetailFetchCreditTermOptions: ['search'],
  slsOrdDetailFetchCreditTermOptionLoading: ['boolean'],
  slsOrdDetailFetchCreditTermOptionSuccess: ['options'],

  slsOrdDetailFetchCurrencyOptions: ['search'],
  slsOrdDetailFetchCurrencyOptionLoading: ['boolean'],
  slsOrdDetailFetchCurrencyOptionSuccess: ['options'],
  slsOrdDetailChangeCurrency: ['formikBag', 'currencyId'],

  slsOrdDetailFetchLocationOptions: ['siteFlowId', 'search'],
  slsOrdDetailFetchLocationOptionLoading: ['boolean'],
  slsOrdDetailFetchLocationOptionSuccess: ['options'],

  slsOrdDetailFetchItemOptions: ['search'],
  slsOrdDetailFetchItemOptionLoading: ['boolean'],
  slsOrdDetailFetchItemOptionSuccess: ['options'],
  slsOrdDetailChangeItem: ['formikBag', 'hdrId', 'itemId'],

  slsOrdDetailFetchUomOptions: ['itemId', 'search'],
  slsOrdDetailFetchUomOptionLoading: ['boolean'],
  slsOrdDetailFetchUomOptionSuccess: ['options'],
  slsOrdDetailChangeUom: ['formikBag', 'hdrId', 'itemId', 'uomId'],

  slsOrdDetailSetDocDate: ['documentHeader'],
  slsOrdDetailSetEstDelDate: ['documentHeader'],

  slsOrdDetailSetErrorMessage: ['errorMessage'],

  slsOrdDetailSetItem: ['hdrId', 'itemId'],
  slsOrdDetailSetItemSuccess: ['resultData'],
  /* Added desc_01 to initial the name(desc_01) */
  slsOrdDetailSetInitDocumentDetail: ['documentDetail', 'item_id', 'desc_01', 'desc_02'],
  slsOrdDetailSetSuccessMessage: ['successMessage'],
  slsOrdDetailSetDocumentDetailByPending: ['documentDetail', 'index'],

  slsOrdDetailFetchAllLocationOptions: ['siteFlowId', 'search', 'page'],
  slsOrdDetailFetchAllLocationOptionSuccess: ['options'],
  searchLocationOptionOffline: null,
  resetCacheLocationsOptions: null,
  changeUomOffline: ['formikBag', 'uomId'],
  resetOptions: null,
});

export const SlsOrdDetailTypes = Types;
export default Creators;
