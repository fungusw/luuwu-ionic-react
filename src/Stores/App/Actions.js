import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  appAuthenticate: ['formikBag', 'username', 'password', 'apiUrl'],
  appAuthenticateSuccess: ['user', 'successMessage', 'apiUrl'],
  appTokenExpired: ['errorMessage'],
  appPasswordExpired: ['errorMessage'],
  setShowSuccessToast: ['showSuccessToast'],
  setShowErrorToast: ['showErrorToast'],
  setErrorMessage: ['errorMessage'],
  resetToInitialState: null,
  goToItem: null,
  goToBack: ['path'],
  appUpdateApiUrl: ['apiUrl'],
  appUpdateAppPath: ['appPath'],
  startUp: null,
  logout: null,
  goToProfile: null,
});

export const AppTypes = Types;
export default Creators;
