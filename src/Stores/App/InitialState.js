/**
 * The initial values for the redux state.
 */
export default {
  username: null,
  password: null,
  successMessage: '',
  errorMessage: '',
  translationLoaded: false,
  token: '',
  apiUrl: 'http://inverze.dyndns.org:8000/scm-backend/public/api/v1',
  appPath: '',
  locale: 'en-US',
  curDivisionId: 1,
  curSiteFlowId: 1,
  user: {
    username: '',
    email: '',
    first_name: '',
    last_name: '',
    timezone: '',
    last_login: '',
    password_changed_at: ''
  },

  siteFlowOptions: [],
  siteFlowIsLoading: false,
  divisionOptions: [],
  divisionIsLoading: false,
  showSuccessToast: false,
  showErrorToast: false,
  };
  