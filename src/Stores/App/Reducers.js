/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { AppTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const appAuthenticateSuccess = (state, { user, successMessage, apiUrl }) => ({
  ...state,
  token: user.token,
  user,
  apiUrl,
  showSuccessToast: true,
  successMessage,
});

export const appTokenExpired = (state, { errorMessage }) => ({
  ...state,
  errorMessage,
});

export const appPasswordExpired = (state, { errorMessage }) => ({
  ...state,
  errorMessage,
});

export const setShowSuccessToast = (state, { showSuccessToast }) => ({
  ...state,
  showSuccessToast,
});

export const setShowErrorToast = (state, { showErrorToast }) => ({
  ...state,
  showErrorToast,
});

export const setErrorMessage = (state, { errorMessage }) => ({
  ...state,
  errorMessage,
  showErrorToast: true,
});

export const resetToInitialState = state => ({
  ...state,
  username: null,
  password: null,
  successMessage: '',
  errorMessage: '',
  translationLoaded: false,
  token: '',
  apiUrl: INITIAL_STATE.apiUrl,
  locale: 'en-US',
  curDivisionId: 1,
  curSiteFlowId: 1,
  user: {
    username: '',
    email: '',
    first_name: '',
    last_name: '',
    timezone: '',
    last_login: '',
    password_changed_at: ''
  },

  siteFlowOptions: [],
  siteFlowIsLoading: false,
  divisionOptions: [],
  divisionIsLoading: false,
  showSuccessToast: false,
  showErrorToast: false,
})

export const appUpdateApiUrl = (state, { apiUrl }) => ({
  ...state,
  apiUrl
});

export const appUpdateAppPath = (state, { appPath }) => ({
  ...state,
  appPath
});

export const logout = (state, { appPath }) => ({
  ...state,
  token: INITIAL_STATE.token,
  user: INITIAL_STATE.user,
  successMessage: 'Logout Success',
  errorMessage: '',
  showSuccessToast: true,
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AppTypes.APP_AUTHENTICATE_SUCCESS]: appAuthenticateSuccess,
  [AppTypes.APP_TOKEN_EXPIRED]: appTokenExpired,
  [AppTypes.APP_PASSWORD_EXPIRED]: appPasswordExpired,
  [AppTypes.SET_SHOW_SUCCESS_TOAST]: setShowSuccessToast,
  [AppTypes.SET_SHOW_ERROR_TOAST]: setShowErrorToast,
  [AppTypes.SET_ERROR_MESSAGE]: setErrorMessage,
  [AppTypes.RESET_TO_INITIAL_STATE]: resetToInitialState,
  [AppTypes.APP_UPDATE_API_URL]: appUpdateApiUrl,
  [AppTypes.APP_UPDATE_APP_PATH]: appUpdateAppPath,
  [AppTypes.LOGOUT]: logout,
});
