## All the component class file are in pages.

- In the /src/pages folder, rename Home.tsx to Home.jsx
- Create Home folder and Move the Home.jsx and Home.css to /src/pages/Home/
- Then replace the file with react web structure 

### Example
```
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {IonPage, IonHeader, IonToolbar, IonTitle, IonContent} from '@ionic/react';

class Home extends PureComponent {
  constructor() {
    super();
  }

  componentDidMount() {}

  render () {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Blank</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Blank</IonTitle>
            </IonToolbar>
          </IonHeader>
        </IonContent>
      </IonPage>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
```